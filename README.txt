NOME:Jair Filipe Peruchi Cardoso
TURMA:3-51

Prova 01 � 1� Trimestre

(1) As estruturas de programa��o das
linguagens permitem criar l�gicas para o fluxo
dos dados de nossos softwares. Assinale a
alternativa que cont�m uma estrutura de
programa��o usada pela linguagem ObjectPascal que permite iterar indefinidamente sobre
um mesmo conjunto de instru��es:
a) IF / ELSE
b) FOR
c) FOREACH
d) DO
e) WHILE

(2) As estruturas de dados presentes nas
linguagens de programa��o s�o necess�rias
para que a aplica��o possa entender e
armazenar corretamente os dados que
pretendemos utilizar. Assinale a alternativa que
cont�m, respectivamente, uma estrutura capaz
de armazenar apenas n�meros inteiros positivos
e negativos e uma estrutura que armazena
apenas dois valores:
a) FLOAT e INTEGER
b) INTEGER e BOOLEAN
c) STRING e INTEGER
d) BOOLEAN e FLOAT
e) STRING e STRING

(3) V�rias linguagens de programa��o permitem
a cria��o de enumeradores. Este tipo de
estrutura de dados permite criar uma vari�vel
que armazena um valor pr�-definido pelo pr�prio
programador. Utilizando como refer�ncia a
linguagem Object-Pascal, assinale a alternativa
que representa a cria��o de um enumerador
para os dias de semana:
a) var diaSemana = (seg, ter, qua, qui, sex);
b) var diaSemana = [seg, ter, qua, qui, sex];
c) new type diaSemana = (seg, ter, qua, qui,
sex);
d) type diaSemana = (seg, ter, qua, qui, sex);
e) string diaSemana = (seg, ter, qua, qui, sex);

(4) Existe uma estrutura definida pelo ObjectPascal capaz de agrupar itens de dados de
diferentes tipos (ao contr�rio do array, que
armazena v�rios itens de mesmo tipo). Assinale
a alternativa que representa o nome desta
estrutura:
a) Array
b) Matrix
c) String
d) Boolean
e) Record

(5) O Git � um sistema de controle de vers�es
desenvolvido por Linus Torvalds e Junio Hamano
que facilita o processo de desenvolvimento de
software ao ser usado para registrar o hist�rico
de edi��es dos arquivos-fonte. Assinale a
alternativa que n�o representa uma fun��o do
Git:
a) Registrar o hist�rico de edi��es de arquivos
em uma pasta/projeto
b) Impedir o acesso n�o autorizado aos arquivos
fonte por meio da autentica��o de usu�rio
c) Facilitar a c�pia e atualiza��o do projeto com
outras fontes atrav�s de reposit�rios remotos
d) Permitir retornar (rollback) a estados
anteriores do desenvolvimento do software
e) Resolver conflitos de modifica��es feitas no
projeto por diferentes programadores

(6) Para organizar a �rea de desenvolvimento, o
Git implementa diversas �reas com diferentes
caracter�sticas dentro de um projeto. Dessa
forma, o Git minimiza altera��es desastrosas
que podem comprometer a integridade do
c�digo. Assinale a alternativa que representa o
nome da �rea do Git onde ficam armazenados
os arquivos que est�o prontos para serem
preservados permanentemente no reposit�rio
local (por�m ainda n�o foram):
a) Stage/Index
b) Stash
c) Local Repository
d) Remote Repository
e) Workspace

(7) Ao realizar altera��es no c�digo, o Git n�o
preserva automaticamente as altera��es
efetuadas em seu reposit�rio. Antes de mais
nada, � necess�rio indicar ao versionador quais
s�o as modifica��es que pretendemos preservar
em uma nova vers�o. Assinale a alternativa que
preserva APENAS as altera��es realizadas no
arquivo stark.php (levando em considera��o
que o arquivo do mesmo projeto
vingadores.php tamb�m possui altera��es):
a) git add .
b) git add stark.php
c) git commit -m �Altera��es realizadas no
homem de ferro�
d) git add vingadores.php
e) git commit -m �stark.php�

(8) Quando h� modifica��es listadas no Index do
reposit�rio Git, podemos preservar
permanentemente estas altera��es em um
processo chamado de commit. Assinale a
alternativa que melhor descreve o
comportamento do comando git commit:
a) Todas as modifica��es s�o preservadas no
reposit�rio local e, em seguida, enviadas ao
reposit�rio remoto
b) Todos os arquivos que n�o foram listados no
Index s�o apagados e preserva-se apenas os
que foram modificados na nova vers�o
c) Todo conte�do do projeto � enviado para um
reposit�rio remoto, como GitHub ou GitLab
d) Todas as altera��es s�o preservadas no
reposit�rio local, de forma que seja imposs�vel
retornar ao estado anterior.
e) Cria-se um identificador �nico para o commit e as modifica��es s�o preservadas no reposit�rio
local

(9) O reposit�rio remoto � uma defini��o do Git
para uma c�pia remota do reposit�rio local de
um determinado projeto. No entanto, estes dois
tipos de reposit�rio podem conter diferen�as
entre si, que necessitam da atualiza��o do
programador. Assinale a alternativa que
representa, respectivamente, o comando
utilizado para atualizar o reposit�rio local (com o
conte�do remoto) e o comando utilizado para
atualizar o reposit�rio remoto (com o conte�do
local)
a) GIT CLONE e GIT UPDATE
b) GIT UPDATE e GIT CLONE
c) GIT PULL e GIT PUSH
d) GIT CLONE e GIT PUSH
e) GIT PUSH e GIT PULL

(10) Os gerenciadores de reposit�rio baseados
em Git (como o GitLab) s�o respons�veis por
permitir que desenvolvedores armazenem
remotamente seus reposit�rios e oferecem
ferramentas para a constru��o do software de
maneira colaborativa. Assinale a alternativa que
apresenta o comando utilizado para realizar a
c�pia de um reposit�rio para a sua m�quina,
levando em considera��o:
Servidor: gitlab.com
Usu�rio: rvenson
Reposit�rio: prova01
a) git clone https://gitlab.com/rvenson/prova01
b) git pull https://gitlab.com/rvenson/prova01
c) git clone gitlab.com rvenson prova01
d) git pull gitlab.com rvenson prova01
e) git pull https://gitlab.com/prova01/rvenson

GABARITO

1- E- O while funciona enquanto uma condi��o for verdadeira,
ou seja, ele pode funcionar ininitamente.

2- B- INTEGER, pois ele armazena somemte n�meros inteiros e
BOOLEAN, pois armazena somete 0 ou 1.

3- D- Pois representa sintaxe correta para criar enumeradores na linguagem Object-Pascal.

4- E- Pois s� o record pode armazernar dados de tipos diferentes juntos.

5- B- Pois o GIT n�o tem fun��o de impedir o acesso por meio de autentica��o de usu�rio.

6- A- Pois o index � a �rea onde as altera��es s�o salvas no reposit�rio local.

7- B- Pois salva o u arquivo espec�fifco. 

8- E Pois a cada commit se cria um identificador �nico

9- C Pois o git pull atualiza o reposit�rio local e git push atualiza o reposit�rio remoto.
local)

10- A Pois � a sintaxe correta para usar o comando git clone.

Boa prova,
Prof. Ramon Venson
b                               